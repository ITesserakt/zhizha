#include "RestartSystem.h"
#include "FontResource.h"
#include "Components/RenderedComponent.h"
#include "Components/LayerComponent.h"
#include "Components/ButtonComponent.h"

void RestartSystem::OnInit() {
	_restartEntity = &_entities->CreateEntity();

	auto& font = _resources.GetOrAddResource<FontResource>("JetBrainsMono-Regular")->Font();

	_restartText.setFont(font);
	_restartText.setPosition(170, 500);
	_restartText.setCharacterSize(64);
	_restartText.setFillColor(sf::Color::Black);
	_restartText.setString("Exit to main menu");

	_entities->GetOrAddComponent<RenderedComponent>(*_restartEntity).DrawableObj = &_restartText;
	_entities->GetOrAddComponent<LayerComponent>(*_restartEntity).Index = Gui;
	_entities->GetOrAddComponent<ButtonComponent>(*_restartEntity, [&](ButtonComponent& c) {
		c.Bounds = sf::IntRect(_restartText.getGlobalBounds());
		c.OnClick = [&] {
			_engine.UnloadScene();
			_engine.LoadScene(Scene::Menu);
		};
		c.OnHover = [&] {
			_restartText.setFillColor(sf::Color{144, 144, 144});
		};
		c.OnHoverEnds = [&] {
			_restartText.setFillColor(sf::Color::Black);
		};
	});
}

void RestartSystem::OnSceneUnload(Scene scene) {
	_entities->DestroyEntity(*_restartEntity);
}
