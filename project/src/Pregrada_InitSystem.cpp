#include "Components/TransformComponent.h"
#include "Components/MaterialComponent.h"
#include "Components/RenderedComponent.h"
#include "Pregrada_InitSystem.h"
#include "Components/MeshComponent.h"
#include <random>
#include <ctime>
#include <Components/SpeedComponent.h>
#include <ComponentDrop.h>
#include <DefinesPhysics.h>
#include "Components/LayerComponent.h"
#include "GlobalRotation_Component.h"
#include "Components/ComplexCollisionComponent.h"

void Pregrada_InitSystem::OnInit()
{
	//Initialize Enityty
	auto _pregrada = &_entities->CreateEntity();

	_entities->GetOrAddComponent<MeshComponent>(*_pregrada, [&](MeshComponent& c) {
		c.Mesh = _resources.GetOrAddResource<MeshResource>("pregrada");
		});

	_entities->GetOrAddComponent<MaterialComponent>(*_pregrada, [&](MaterialComponent& c) {
		c.VertexShader = _resources.GetOrAddResource<VertexShaderResource>("Map");
		c.FragmentShader = _resources.GetOrAddResource<FragmentShaderResource>("Map");
		c.Textures.emplace_back(_resources.GetOrAddResource<TextureResource>("Tree_2_Albedo.png"));
		c.attributesCount = 2;
		});

	_entities->GetOrAddComponent<TransformComponent>(*_pregrada, [&](TransformComponent& c) {
		c.Location = { 0.0, -.2 };
		c.Scale = { 0.07, 0.07 };
		c.Angle = 0.0f;
		c.parent = nullptr;
		});

	_entities->GetOrAddComponent<LayerComponent>(*_pregrada, [](LayerComponent& c) {
		c.Index = Game;
	});

	_entities->GetOrAddComponent<GlobalRotation_Component>(*_pregrada);
	_entities->GetOrAddComponent<ComplexCollisionComponent>(*_pregrada);
}
