#include <ResourceFile.h>
#include <SFML/Graphics/Font.hpp>
#include <Components/RenderedComponent.h>
#include <Components/LayerComponent.h>
#include <FontResource.h>
#include "CountdownSystem.h"
#include "ComponentDrop.h"

void CountdownSystem::OnUpdate() {
	int left = _entities->GetEntitiesBy<ComponentDrop>().size() - 5;

	if (left / _maxDrops < 0.1)
		_text.setFillColor(sf::Color{232, 39, 39});
	else if (left / _maxDrops < 0.2)
		_text.setFillColor(sf::Color{232, 143, 39});
	else if (left / _maxDrops < 0.3)
		_text.setFillColor(sf::Color{220, 232, 39});
	else if (left / _maxDrops < 0.4)
		_text.setFillColor(sf::Color{95, 232, 39});

	_text.setString("Drops left: " + std::to_string(left));
}

void CountdownSystem::OnInit() {
	auto& font = _resources.GetOrAddResource<FontResource>("JetBrainsMono-Regular")->Font();
	_textEntity = &_entities->CreateEntity();
	_entities->GetOrAddComponent<RenderedComponent>(*_textEntity, [&](RenderedComponent& c) {
		_text.setFont(font);
		_text.setCharacterSize(20);
		_text.setFillColor(sf::Color{43, 232, 39});
		_text.setPosition(0, 60);
		c.DrawableObj = &_text;
	});
	_entities->GetOrAddComponent<LayerComponent>(*_textEntity, [](LayerComponent& c) {
		c.Index = Gui;
	});
	_maxDrops = _entities->GetEntitiesBy<ComponentDrop>().size();
}

void CountdownSystem::OnSceneUnload(Scene scene) {
	_entities->DestroyEntity(*_textEntity);
}
