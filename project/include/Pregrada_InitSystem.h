#pragma once

#include "ResourceManager.h"
#include "System.h"
#include "SFML/Graphics/RenderWindow.hpp"

class Pregrada_InitSystem : public virtual InitSystem {
	ResourceManager& _resources;

public:
	explicit Pregrada_InitSystem(ResourceManager& resources) : _resources(resources) {};

	void OnInit() override;
};