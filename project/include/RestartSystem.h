#pragma once

#include "System.h"
#include "ResourceManager.h"
#include "Engine.h"
#include "SFML/Graphics.hpp"

class RestartSystem: public virtual InitSystem, public virtual UnloadSystem {
	Entity* _restartEntity;
	Engine& _engine;
	ResourceManager& _resources = _engine.GetResourceManager();
	sf::Text _restartText;

public:
	explicit RestartSystem(Engine& engine) : _engine(engine) { }

	void OnInit() override;
	void OnSceneUnload(Scene scene) override;
};