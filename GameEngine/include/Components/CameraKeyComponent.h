#pragma once

#include "Component.h"
#include "SFML/Graphics.hpp"

struct CameraKeyComponent : public ComponentData<CameraKeyComponent> {
	sf::Vector2f CurrentPoint;

	CameraKeyComponent * Next;
};