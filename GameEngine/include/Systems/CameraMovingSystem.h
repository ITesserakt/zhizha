#pragma once

#include <Components/CameraKeyComponent.h>
#include "System.h"
#include "SFML/Graphics.hpp"
#include <list>

class CameraMovingSystem : public virtual FixedUpdateSystem, public virtual UnloadSystem {
	sf::Vector2f& _camera_location;
	std::list<sf::Vector2f> _trajectory;

	float _stepsInSec = 300;
	float _currentStep = 0;

	[[nodiscard]] sf::Vector2f lerp(sf::Vector2f a, sf::Vector2f b, float t) const;

public:
	explicit CameraMovingSystem(sf::Vector2f& camera_location)
			:_camera_location(camera_location) {
		_trajectory.emplace_back(-0.036, -0.320);
		_trajectory.emplace_back(-0.042, -2.125);
		_trajectory.emplace_back(-1.314, -3.329);
		_trajectory.emplace_back(-4.269, -3.486);
		_trajectory.emplace_back(-4.572, -4.469);
		_trajectory.emplace_back(-6.408, -4.686);
	}

	void OnFixedUpdate() override;
	void OnSceneUnload(Scene scene) override;
};