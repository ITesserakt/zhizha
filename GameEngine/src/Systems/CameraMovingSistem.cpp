#include <Components/TransformComponent.h>
#include "Systems/CameraMovingSystem.h"
#include "utils.h"
#include "../../../PhysicsEngine/include/ComponentDrop.h"

using namespace sf::Extensions::Vector2;

void CameraMovingSystem::OnFixedUpdate() {
	if (_trajectory.size() < 2)
		return;

	_camera_location = lerp(*_trajectory.begin(), *(++_trajectory.begin()),
			(_currentStep++) / _stepsInSec);
	if (_currentStep >= _stepsInSec) {
		_currentStep = 0;
		_trajectory.pop_front();
	}

	const auto& items = _entities->GetEntitiesBy<TransformComponent, ComponentDrop>();
	for (auto&[components, current_entity] : items) {
		auto&[currentTransform, currentDrop] = components;
		if (std::abs(currentTransform->Location.x + _camera_location.x) > 1.5f ||
				currentTransform->Location.y + _camera_location.y > 1.5f ||
				currentTransform->Location.y + _camera_location.y < -2.f) {
			_entities->DestroyEntity(*current_entity);
		}
	}
}

sf::Vector2f CameraMovingSystem::lerp(sf::Vector2f a, sf::Vector2f b, float t) const {
	float xn = a.x + (b.x - a.x) * t;
	float yn = a.y + (b.y - a.y) * (xn - a.x) / (b.x - a.x);

	return sf::Vector2f{ xn, yn };
}

void CameraMovingSystem::OnSceneUnload(Scene scene) {
	_trajectory.clear();
	_trajectory.emplace_back(0, 0);
	_trajectory.emplace_back(-0.036, -0.320);
	_trajectory.emplace_back(-0.042, -2.125);
	_trajectory.emplace_back(-1.314, -3.329);
	_trajectory.emplace_back(-4.269, -3.486);
	_trajectory.emplace_back(-4.572, -4.469);
	_trajectory.emplace_back(-6.408, -4.686);
}
