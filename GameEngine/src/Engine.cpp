#include <thread>
#include <iostream>
#include "Engine.h"
#include "glad/glad.h"

void Engine::initRenderThread() {
	_window.setActive(true);

	glOrtho(-1.4, 1.4, -1.4, 1.4, -1, 8);
	while (_window.isOpen() && _isActive) {
		_mutex.lock();
		_window.clear(sf::Color { 0, 0, 0, 255 });
		_systemManager->Update();
		_systemManager->PostUpdate();
		_window.display();
		_mutex.unlock();
		_time->waitForUpdate();
	}

	_window.setActive(false);
}

void Engine::initFixedUpdateThread() {
	while (_window.isOpen() && _isActive) {
		_mutex.lock();
		_systemManager->FixedUpdate();
		_mutex.unlock();
		_time->waitForFixedUpdate();
	}
}

Engine::~Engine() {
	delete _systemManager;
	delete _resourceManager;
	delete _entityManager;
}

void Engine::Start() {
	_isActive = true;

	std::thread update([&] { initRenderThread(); });
	std::thread fixed([&] { initFixedUpdateThread(); });

	_systemManager->PostInit();
	update.join();
	_window.setActive(true);
	fixed.join();
	_window.close();
}

void Engine::LoadScene(Scene scene) {
	_currentScene = scene;
	std::cout << "Loading scene #" << static_cast<int>(scene) << "... ";
	_systemManager->ActivateInitSystems(scene);
	_window.setActive(true);
	_systemManager->Init();
	_window.setActive(false);

	_systemManager->ActivateOtherSystems(scene);
	std::cout << "Done" << std::endl;
}

void Engine::UnloadScene() {
	_time->reset();
	_systemManager->UnloadScene(_currentScene);
}

void Engine::Stop() {
	UnloadScene();
	_isActive = false;
}